import mysql.connector

mydb = mysql.connector.connect(
  host="mysql-service",
  port=3306,
  user="root",
  password="root",
  database="lr_2_db"
)

mycursor = mydb.cursor()

mycursor.execute("""
SELECT subject, count(gradebook_id) FROM gradebooks
WHERE mark IS NOT NULL
GROUP BY subject;
""")

header = [row[0] for row in mycursor.description]

result = mycursor.fetchall()
print(result)

f = open('output/result2.csv', 'w')
head = ','.join(header)
f.write(str(head + '\n'))
for row in result:
    f.write(','.join(str(r) for r in row) + '\n')
f.close()

mydb.close()
